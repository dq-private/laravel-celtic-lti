<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\SetList;
use Rector\Laravel\Set\LaravelSetList;

return static function (RectorConfig $rectorConfig): void {

//    $rectorConfig->parallel();

    // paths to refactor; solid alternative to CLI arguments
    $rectorConfig->paths([
        __DIR__ . '/config',
        __DIR__ . '/database',
        __DIR__ . '/routes',
        __DIR__ . '/src',
    ]);
    $rectorConfig->autoloadPaths([
        // discover specific file
        __DIR__ . '/vendor/autoload.php',
    ]);

    $rectorConfig->importNames();
    $rectorConfig->import(SetList::PHP_74);
    $rectorConfig->import(LaravelSetList::LARAVEL_80);
    $rectorConfig->import(SetList::TYPE_DECLARATION);
//    $rectorConfig->import(SetList::CODING_STYLE);
//    $rectorConfig->import(SetList::CODE_QUALITY);
    $rectorConfig->skip([
        Rector\CodingStyle\Rector\ClassConst\VarConstantCommentRector::class,
        Rector\TypeDeclaration\Rector\ClassMethod\AddArrayParamDocTypeRector::class,
        Rector\TypeDeclaration\Rector\ClassMethod\AddArrayReturnDocTypeRector::class,
        Rector\TypeDeclaration\Rector\Property\CompleteVarDocTypePropertyRector::class,
        //Rector\CodeQuality\Rector\Isset_\IssetOnPropertyObjectToPropertyExistsRector::class,
        //Rector\CodingStyle\Rector\Encapsed\EncapsedStringsToSprintfRector::class,
    ]);

};
