# Laravel Celtic LTI package

This is a Laravel wrapper for Celtic LTI php https://github.com/celtic-project/LTI-PHP

## Installation

You can install the package via composer:

```bash
composer require dolphiq/laravel-celtic-lti
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="Dolphiq\LaravelCelticLti\LaravelCelticLtiServiceProvider" --tag="laravel_celtic_lti-migrations"
php artisan migrate
```

You can publish the config file with:
```bash
php artisan vendor:publish --provider="Dolphiq\LaravelCelticLti\LaravelCelticLtiServiceProvider" --tag="laravel_celtic_lti-config"
```

## Usage
https://github.com/celtic-project/LTI-PHP/wiki

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.
