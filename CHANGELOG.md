# Changelog

All notable changes to `laravel-celtic-lti` will be documented in this file.

## 1.0.0 - 202X-XX-XX

- initial release
