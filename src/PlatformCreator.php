<?php

namespace Dolphiq\LaravelCelticLTI;

use ceLTIc\LTI\DataConnector\DataConnector;
use ceLTIc\LTI\Platform;
use RuntimeException;
use ceLTIc\LTI\Util;
use ceLTIc\LTI;

class PlatformCreator
{
    public static function createLTI1p2Platform(
        string $consKey,
        DataConnector $dataConnector,
        string $name,
        string $secret
    ): void {
        $platform = Platform::fromConsumerKey($consKey, $dataConnector);
        if ($platform->created !== null) {
            throw new RuntimeException("Platform with this key already exists. Refusing to overwrite.");
        }
        $platform->name = $name;
        $platform->secret = $secret;
        $platform->enabled = true;
        $platform->save();
    }

    public static function createLTI1_3_0Platform(
        string $consKey,
        DataConnector $dataConnector,
        string $name,
        string $secret,
        string $signatureMethod,
        string $platformId,
        string $clientId,
        string $deploymentId,
        string $authenticationUrl,
        string $accessTokenUrl,
        string $jku
    ): void {
        $platform = Platform::fromConsumerKey($consKey, $dataConnector);
        if ($platform->created !== null) {
            throw new RuntimeException("Platform with this key already exists. Refusing to overwrite.");
        }

        $platform->ltiVersion = Util::LTI_VERSION1P3;
        $platform->signatureMethod = $signatureMethod; //'RS256';
        $platform->setKey($consKey);
        $platform->name = $name;
        $platform->secret = $secret;
        $platform->platformId = $platformId;
        $platform->clientId = $clientId;
        $platform->deploymentId = $deploymentId;
        $platform->authorizationServerId = null;
        $platform->authenticationUrl = $authenticationUrl;
        $platform->accessTokenUrl = $accessTokenUrl;
        $platform->jku = $jku;
        $platform->enabled = true;
        $platform->save();
    }
}
