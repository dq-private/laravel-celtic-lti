<?php

namespace Dolphiq\LaravelCelticLti;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Dolphiq\LaravelCelticLti\LaravelCelticLti
 */
class LaravelCelticLtiFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'laravel_celtic_lti';
    }
}
