<?php

namespace Dolphiq\LaravelCelticLti\Commands;

use ceLTIc\LTI\DataConnector\DataConnector;
use ceLTIc\LTI\Jwt\FirebaseClient;
use ceLTIc\LTI\Util;
use Dolphiq\LaravelCelticLti\PlatformCreator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LaravelCelticLtiCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'lti:add-platform';
    /**
     * @var string
     */
    protected $description = 'Add a new LTI platform.';

    public function handle(): int
    {
        $ltiVersion = $this->choice(
            'Which lti version do you want to use?',
            [Util::LTI_VERSION1, Util::LTI_VERSION1P3]
        );

        switch ($ltiVersion) {
            case Util::LTI_VERSION1:
                $this->executeLti_v1_1();
                break;
            case Util::LTI_VERSION1P3:
                $this->executeLti_v1_3();
                break;
            default:
                $this->error('Lti version not found');
        }
        return 0;
    }

    private function executeLti(): array
    {
        $name = $this->ask('What is the platform name?');
        $consKey = $this->ask('What is the key name?');
        $secret = $this->ask('What is the secret (leave empty to generate a random string)');

        if (empty($secret)) {
            $secret = Str::random(40);
        }

        return [$name, $consKey, $secret];
    }


    private function executeLti_v1_1(): void
    {
        $pdo = DB::connection()->getPdo();
        $dataConnector = DataConnector::getDataConnector($pdo, '', 'pdo');

        [$name, $consKey, $secret] = $this->executeLti();

        PlatformCreator::createLTI1p2Platform($consKey, $dataConnector, $name, $secret);

        $this->info("Successfully created LTI integration as \'" . $name . "\'");
    }

    private function executeLti_v1_3(): void
    {
        $pdo = DB::connection()->getPdo();
        $dataConnector = DataConnector::getDataConnector($pdo, '', 'pdo');

        [$name, $consKey, $secret] = $this->executeLti();

        $signatureMethod = $this->choice('What signature method do you use?', FirebaseClient::SUPPORTED_ALGORITHMS);
        $platformId = $this->ask('What is the platform id?');
        $clientId = $this->ask('What is the client id?');
        $deploymentId = $this->ask('What is the deployment id?');
        $authenticationUrl = $this->ask('What is the authentication url?');
        $accessTokenUrl = $this->ask('What is the access token url?');
        $jku = $this->ask('What is the jku (url)?');

        PlatformCreator::createLTI1_3_0Platform(
            $consKey,
            $dataConnector,
            $name,
            $secret,
            $signatureMethod,
            $platformId,
            $clientId,
            $deploymentId,
            $authenticationUrl,
            $accessTokenUrl,
            $jku
        );

        $this->info("Successfully created LTI integration as \'" . $name . "\'");
    }
}
