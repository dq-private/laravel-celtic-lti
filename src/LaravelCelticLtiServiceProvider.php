<?php

namespace Dolphiq\LaravelCelticLti;

use Dolphiq\LaravelCelticLti\Commands\LaravelCelticLtiCommand;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use ReflectionClass;

class LaravelCelticLtiServiceProvider extends ServiceProvider
{

    public array $migrationFileNames = [
        'create_laravel_celtic_lti_table',
    ];

    public array $routeFileNames = [
        // 'celtic_lti'
    ];

    public function register(): void
    {
        $this->mergeConfigFrom($this->basePath("/../config/celtic_lti.php"), 'celtic_lti');
    }

    public function boot(): self
    {
        if ($this->app->runningInConsole()) {
            $this->publishes(
                [$this->basePath("/../config/celtic_lti.php") => config_path("celtic_lti.php"),],
                "celtic_lti-config"
            );
            $now = Carbon::now();
            foreach ($this->migrationFileNames as $migrationFileName) {
                if (!$this->migrationFileExists($migrationFileName)) {
                    $this->publishes(
                        [
                            $this->basePath("/../database/migrations/{$migrationFileName}.php.stub") => database_path(
                                'migrations/' . $now->addSecond()->format('Y_m_d_His') . '_' . Str::finish(
                                    $migrationFileName,
                                    '.php'
                                )
                            ),
                        ],
                        "celtic_lti-migrations"
                    );
                }
            }
        }

        $this->commands(LaravelCelticLtiCommand::class);

        foreach ($this->routeFileNames as $routeFileName) {
            $this->loadRoutesFrom("{$this->basePath('/../routes/')}{$routeFileName}.php");
        }

        return $this;
    }

    public static function migrationFileExists(string $migrationFileName): bool
    {
        $len = strlen($migrationFileName) + 4;

        foreach (glob(database_path("migrations/*.php")) as $filename) {
            if ((substr($filename, -$len) === $migrationFileName . '.php')) {
                return true;
            }
        }

        return false;
    }

    public function basePath(string $directory = null): string
    {
        if ($directory === null) {
            return $this->getPackageBaseDir();
        }

        return $this->getPackageBaseDir() . DIRECTORY_SEPARATOR . ltrim($directory, DIRECTORY_SEPARATOR);
    }

    protected function getPackageBaseDir(): string
    {
        $reflector = new ReflectionClass(get_class($this));

        return dirname($reflector->getFileName());
    }
}
